# Defect Registry

## Launch the app - 1

Follow steps below in order to launch the application:

1. ```cd president-election-app```
2. ```mvn clean package```
3. ```java -jar target//presidentElection-1.0-SNAPSHOT.jar```

## Launch the app - 2

Follow steps below in order to launch the application using docker && kubernetes:

1. ```cd president-election-app```
2. ```docker build -t president-election .```
3. ```kubectl apply -f president-election.yaml```

## How to use the API

## URL: localhost:8080


### GET

```
  {url}/voters
  
  [
      {
          "id": 1,
          "region": "Normandie",
          "vote": 1
      },
      {
          "id": 2,
          "region": "Bretagne",
          "vote": 3
      }
  ]
```
```
  {url}/candidates
  
  [
      {
          "id": 1,
          "name": "John Johnson",
          "summary": "John's summary"
      },
      {
          "id": 2,
          "name": "Mike Mikenson",
          "summary": "Mike's summary"
      }
  ]
```
```
  {url}/results/overall
  
  [
      {
          "votes": 2,
          "candidateId": "1"
      },
      {
          "votes": 3,
          "candidateId": "2"
      }
  ]
```
```
  {url}/results/regions
  
  {   
      "Bretagne": {
          {
          "votes": 2,
          "candidateId": "1"
          },
          {
              "votes": 3,
              "candidateId": "2"
          }
      },
      "Grand Est": {
          {
              "votes": 3,
              "candidateId": "2"
          }
      }
  }
```
```
  {url}/results/winner
  
  [
      {
          "votes": 3,
          "candidateId": "2"
      },
      {
          "votes": 2,
          "candidateId": "1"
      }
  ]
```

### POST

```
  {url}/voters
  '{
    "id": 1,
    "region": "California",
    "vote": 3
  }'

```
```
  {url}/candidates
  '{
      "id": 1,
      "name": "John Johnson",
      "summary": "John's Summary"
   }'

```
