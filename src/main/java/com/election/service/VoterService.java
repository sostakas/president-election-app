package com.election.service;

import com.election.dao.VoterDao;
import com.election.model.Voter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import java.util.Collection;

@Service
@Qualifier("fakeVotersData")
public class VoterService {

    @Autowired
    private VoterDao voterDao;

    public Collection<Voter> getAllVoters() {
        return voterDao.getAllVoters();
    }

    public void insertVoter(Voter voter) {
        voterDao.insertVoter(voter);
    }
}
