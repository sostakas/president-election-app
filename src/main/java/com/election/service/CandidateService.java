package com.election.service;

import com.election.dao.CandidateDao;
import com.election.model.Candidate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import java.util.Collection;

@Service
@Qualifier("fakeCandidatesData")
public class CandidateService {

    @Autowired
    private CandidateDao candidateDao;

    public Collection<Candidate> getAllCandidates() {
        return candidateDao.getAllCandidates();
    }

    public void insertCandidate(Candidate candidate) {
        candidateDao.insertCandidate(candidate);
    }
}
