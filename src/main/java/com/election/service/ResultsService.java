package com.election.service;

import com.election.dao.ResultsDao;
import com.election.model.ResultsOverall;
import com.election.model.ResultsRegion;
import com.election.model.ResultsCandidate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;


@Service
public class ResultsService {

    @Autowired
    private ResultsDao resultsDao;

    public Collection<ResultsOverall> getResultsOverall() {
        return resultsDao.getResultsOverall();
    }

    public Collection<ResultsRegion> getResultsByRegion() {
        return resultsDao.getResultsByRegion();
    }

    public Collection<ResultsCandidate> getWinner() {
        return resultsDao.getWinner();
    }
}