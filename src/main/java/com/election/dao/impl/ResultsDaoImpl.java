package com.election.dao.impl;

import com.election.dao.ResultsDao;
import com.election.dao.VoterDao;
import com.election.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.stream.Collectors;

@Repository
public class ResultsDaoImpl implements ResultsDao {

    private static Map<Integer, ResultsOverall> resultsOverall = new HashMap<>();
    private static Map<Integer, ResultsCandidate> resultsWinner = new HashMap<>();

    @Autowired
    private VoterDao voterDao;

    public Collection<ResultsCandidate> getWinner() {
        return calculateWinner();
    }

    public Collection<ResultsOverall> getResultsOverall() {

        return calculateResultsOverall();
    }

    public Collection<ResultsRegion> getResultsByRegion() {

        return calculateResultsByRegion().values();
    }

    private Collection<ResultsOverall> calculateResultsOverall() {
        Map<Integer, Integer> overallVotes = calculateOverallVotes();

        for (Map.Entry<Integer, Integer> entry : overallVotes.entrySet()) {
            resultsOverall.put(entry.getKey(), new ResultsOverall(entry.getKey().toString(), entry.getValue()));
        }
        return this.resultsOverall.values();
    }

    private Map<String, ResultsRegion> calculateResultsByRegion() {

        Map<Integer, Voter> votersHm = voterDao.getMapOfVoters();
        List<Integer> tempListOfRegions = new ArrayList<>();
        Map<Integer, Integer> votesForCandidate = new HashMap<>();
        Map<String, ResultsRegion> resultsRegion = new HashMap<>();
        Map<Integer, ResultsCandidate> resultsCandidate = new HashMap<>();

        int count = 0;
        for (String region : voterDao.getUniqueRegions()) {
            for (Map.Entry<Integer, Voter> entry : votersHm.entrySet()) {
                if (region.equals(entry.getValue().getRegion())) {
                    tempListOfRegions.add(entry.getValue().getVote());
                }
            }

            for (Integer i : tempListOfRegions) {
                Integer j = votesForCandidate.get(i);
                votesForCandidate.put(i, (j == null) ? 1 : j + 1);

            }

            for (Map.Entry<Integer, Integer> entry : votesForCandidate.entrySet()) {
                resultsCandidate.put(count, new ResultsCandidate(entry.getKey().toString(), entry.getValue()));
                count++;
            }

            Collection<ResultsCandidate> values = resultsCandidate.values();

            ArrayList<ResultsCandidate> resultsCandidates = new ArrayList<>(values);

            resultsRegion.put(region, new ResultsRegion(region, resultsCandidates));

            resultsCandidate.clear();
            votesForCandidate.clear();
            tempListOfRegions.clear();
        }

        return resultsRegion;
    }

    private Collection<ResultsCandidate> calculateWinner() {
        float votesCount = voterDao.getAllVoters().size();
        Map<Integer, Integer> winners = calculateOverallVotes();

        if (Integer.parseInt(winners.values().toArray()[0].toString()) >= (votesCount / 2)) {
            resultsWinner.put(0, new ResultsCandidate(winners.keySet().toArray()[0].toString(), Integer.parseInt(winners.values().toArray()[0].toString())));
            resultsWinner.remove(1);
        } else {
            resultsWinner.put(0, new ResultsCandidate(winners.keySet().toArray()[0].toString(), Integer.parseInt(winners.values().toArray()[0].toString())));
            resultsWinner.put(1, new ResultsCandidate(winners.keySet().toArray()[1].toString(), Integer.parseInt(winners.values().toArray()[1].toString())));
        }

        return this.resultsWinner.values();
    }

    private Map<Integer, Integer> calculateOverallVotes() {
        Map<Integer, Integer> overallVotes = new HashMap<>();
        for (Integer i : voterDao.getVotes()) {
            Integer j = overallVotes.get(i);
            overallVotes.put(i, (j == null) ? 1 : j + 1);
        }

        return sortByValue(overallVotes);
    }

    private static Map<Integer, Integer> sortByValue(final Map<Integer, Integer> valueCounts) {
        return valueCounts.entrySet()
                .stream()
                .sorted((Map.Entry.<Integer, Integer>comparingByValue().reversed()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
    }

}
