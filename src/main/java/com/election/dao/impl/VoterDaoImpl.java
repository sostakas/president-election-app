package com.election.dao.impl;

import com.election.dao.CandidateDao;
import com.election.dao.VoterDao;
import com.election.model.Voter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.stream.Collectors;

@Repository
@Qualifier("fakeDataVoter")
public class VoterDaoImpl implements VoterDao {

    private static Map<Integer, Voter> voters;

    @Autowired
    private CandidateDao candidateDao;

    static {

        voters = new HashMap<Integer, Voter>() {

            {
                put(1, new Voter(1, "Normandie", 1));
                put(2, new Voter(2, "Bretagne", 3));
                put(3, new Voter(3, "Paca", 3));
                put(4, new Voter(4, "Grand Est", 1));
                put(5, new Voter(5, "Normandie", 2));
                put(6, new Voter(6, "Paca", 2));
                put(7, new Voter(7, "Paca", 2));


            }
        };
    }

    public Collection<Voter> getAllVoters() {
        return this.voters.values();
    }

    public void insertVoter(Voter voter) {

        if ((!voters.containsKey(voter.getId())) && (candidateDao.getIds().contains(voter.getVote()))) {
            this.voters.put(voter.getId(), voter);
        }
    }

    public List<Integer> getVotes() {
        ArrayList<Integer> names = new ArrayList<>();
        for (Voter voter : voters.values()) {
            names.add(voter.getVote());
        }
        return names;
    }

    public Map<Integer, Voter> getMapOfVoters() {
        return this.voters;
    }

    public List<String> getUniqueRegions() {
        ArrayList<String> regions = new ArrayList<>();
        for (Voter voter : voters.values()) {
            regions.add(voter.getRegion());
        }
        return regions.stream()
                .distinct()
                .collect(Collectors.toList());
    }
}
