package com.election.dao.impl;

import com.election.dao.CandidateDao;
import com.election.model.Candidate;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
@Qualifier("fakeCandidatesData")
public class CandidateDaoImpl implements CandidateDao {

    private static Map<Integer, Candidate> candidates;

    static {

        candidates = new HashMap<Integer, Candidate>() {
            {
                put(1, new Candidate(1, "John Johnson", "John's summary"));
                put(2, new Candidate(2, "Mike Mikenson", "Mike's summary"));
                put(3, new Candidate(3, "Arthur Arthurson", "Arthur's summary"));
            }
        };
    }

    public Collection<Candidate> getAllCandidates() {
        return this.candidates.values();
    }

    public void insertCandidate(Candidate candidate) {
        this.candidates.put(candidate.getId(), candidate);
    }

    public List<Integer> getIds() {
        ArrayList<Integer> ids = new ArrayList<>();
        for (Candidate candidate : candidates.values()) {
            ids.add(candidate.getId());
        }
        return ids;
    }
}