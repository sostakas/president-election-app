package com.election.dao;

import com.election.model.Candidate;

import java.util.Collection;
import java.util.List;

public interface CandidateDao {
    Collection<Candidate> getAllCandidates();
    void insertCandidate(Candidate candidate);
    List<Integer> getIds();
}
