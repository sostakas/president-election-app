package com.election.dao;

import com.election.model.ResultsOverall;
import com.election.model.ResultsRegion;
import com.election.model.ResultsCandidate;

import java.util.Collection;

public interface ResultsDao {
    Collection<ResultsOverall> getResultsOverall();

    Collection<ResultsRegion> getResultsByRegion();

    Collection<ResultsCandidate> getWinner();
}
