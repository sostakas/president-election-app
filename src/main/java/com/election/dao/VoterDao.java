package com.election.dao;

import com.election.model.Voter;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface VoterDao {
    Collection<Voter> getAllVoters();
    void insertVoter(Voter voter);
    List<Integer> getVotes();
    Map<Integer, Voter> getMapOfVoters();
    List<String> getUniqueRegions();
}
