package com.election.model;

import java.util.ArrayList;

public class ResultsRegion {
    private String region;
    private ArrayList<ResultsCandidate> results;

    public ResultsRegion(String region, ArrayList<ResultsCandidate> results) {
        this.region = region;
        this.results = results;
    }

    public ResultsRegion() {}

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public ArrayList<ResultsCandidate> getResults() {
        return results;
    }

    public void setResults(ArrayList<ResultsCandidate> results) {
        this.results = results;
    }
}
