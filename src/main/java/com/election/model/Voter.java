package com.election.model;

public class Voter {

    private int id;
    private String region;
    private int vote;

    public Voter(int id, String region, int vote) {
        this.id = id;
        this.region = region;
        this.vote = vote;
    }

    public Voter() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public int getVote() {
        return vote;
    }

    public void setVote(int vote) {
        this.vote = vote;
    }
}
