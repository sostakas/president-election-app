package com.election.model;

public class ResultsOverall {

    private int votes;
    private String candidateId;

    public ResultsOverall(String candidateId, int votes) {
        this.votes = votes;
        this.candidateId = candidateId;
    }

    public ResultsOverall() {
    }

    public int getVotes() {
        return votes;
    }

    public void setVotes(int votes) {
        this.votes = votes;
    }

    public String getCandidateId() {
        return candidateId;
    }

    public void setCandidateId(String candidateId) {
        this.candidateId = candidateId;
    }

}