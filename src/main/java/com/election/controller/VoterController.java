package com.election.controller;

import com.election.model.Voter;
import com.election.service.VoterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequestMapping("/voters")
public class VoterController {

    @Autowired
    private VoterService voterService;

    @RequestMapping(method = RequestMethod.GET)
    public Collection<Voter> getAllVoters() {
        return voterService.getAllVoters();
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void insertVote(@RequestBody Voter voter) {
        voterService.insertVoter(voter);
    }
}
