package com.election.controller;

import com.election.model.ResultsOverall;
import com.election.model.ResultsRegion;
import com.election.model.ResultsCandidate;
import com.election.service.ResultsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequestMapping("/results")
public class ResultsController {

    @Autowired
    private ResultsService resultsService;

    @RequestMapping(value = "/overall", method = RequestMethod.GET)
    public Collection<ResultsOverall> getResultsOverall() {
        return resultsService.getResultsOverall();
    }

    @RequestMapping(value = "/regions", method = RequestMethod.GET)
    public Collection<ResultsRegion> getResultsByRegion() {
        return resultsService.getResultsByRegion();
    }

    @RequestMapping(value = "/winner", method = RequestMethod.GET)
    public Collection<ResultsCandidate> getWinner() {
        return resultsService.getWinner();
    }
}